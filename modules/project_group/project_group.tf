# To create the subgroup
resource "gitlab_group" "sub_group" {
  name             = var.project_group
  path             = var.project_group_path
  parent_id        = var.parent_group
  visibility_level = var.visibility
  description      = var.description
}