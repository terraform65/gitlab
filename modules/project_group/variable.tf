variable "project_group" {
  type        = string
  description = "The project group name."
}

variable "project_group_path" {
  type        = string
  description = "The project group path."
}

variable "parent_group" {
  type        = string
  description = "The project group parent id."
}

variable "description" {
  type        = string
  description = "The project group parent id."
  default     = ""
}

variable "visibility" {
  type        = string
  default     = "private"
  description = "The project group Visibility."

  validation {
    condition     = var.visibility == "private" || var.visibility == "public" || var.visibility == "internal"
    error_message = "The visibility must be private, public or internal."
  }
}