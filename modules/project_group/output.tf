output "group_id" {
  value       = gitlab_group.sub_group.id
  description = "To get the subgroup id to create project or subgroup in the group"
}