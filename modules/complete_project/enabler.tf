module "my_enabler_sub_group" {
  source          = "../enabler_project"
  parent_group    = var.parent
  visibility = var.visibility
  GITLAB_TOKEN    = var.GITLAB_TOKEN
  TEMPLATE_GITLAB_TOKEN=var.GITLAB_TOKEN
  GITLAB_TEMPLATE_URL="gitlab.com"
  GITLAB_SERVER_URL="gitlab.com"
  GITLAB_BASE_URL = var.GITLAB_BASE_URL
  CI_PROJECT_PATH = module.my_infra_sub_group.ci_project_path
  MAVEN_GROUP_ID = var.MAVEN_GROUP_ID
  enablers = var.enablers
}