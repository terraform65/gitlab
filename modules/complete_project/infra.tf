module "my_infra_sub_group" {
  source          = "../infra_project"
  parent_group    = var.parent
  visibility = var.visibility
  GITLAB_TOKEN    = var.GITLAB_TOKEN
  TEMPLATE_GITLAB_TOKEN=var.TEMPLATE_GITLAB_TOKEN
  GITLAB_TEMPLATE_URL="gitlab.com"
  GITLAB_SERVER_URL="gitlab.com"
  GITLAB_BASE_URL = var.GITLAB_BASE_URL
}