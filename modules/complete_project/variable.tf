variable "visibility" {
  description = "The project visibility"
}

# Gitlab config
variable "GITLAB_TOKEN" {
  default     = "< GITLAB_TOKEN >"
  description = "The gitlab connection token"
}
variable "TEMPLATE_GITLAB_TOKEN" {
  default     = "< GITLAB_TOKEN >"
  description = "The gitlab template connection token"
}
variable "GITLAB_BASE_URL" {
  default     = "https://my.gitlab.server/api/v4/"
  description = "The gitlab API url"
}
variable "GITLAB_TEMPLATE_URL" {
  default     = "gitlab.com"
  description = "The gitlab template url"
}
variable "GITLAB_SERVER_URL" {
  default     = "gitlab.com"
  description = "The gitlab url"
}

variable "MAVEN_GROUP_ID" {
}
variable "enablers" {
  type = list(string)
}
variable "parent" {
}