# The infra group
module "infra_sub_group" {
  source             = "../project_group"
  project_group      = "infra"
  project_group_path = "infra"
  parent_group       = var.parent_group
  visibility         = var.visibility
  description        = "The infrastructure sub group"
}