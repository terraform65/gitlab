output "ci_project_path" {
  value       = module.ci_project.project_path
  description = "The path of the ci project to use it for the enablers"
}