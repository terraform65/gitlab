# the Ci project
module "ci_project" {
  source                = "../project"
  project_group         = module.infra_sub_group.group_id
  project_name          = "CI"
  visibility            = var.visibility
  project_description   = "The CI project"
  pipeline_enabled      = false
  project_template_id   = "25633559"
  global_project_name   = "test"
  GITLAB_TOKEN          = var.GITLAB_TOKEN
  TEMPLATE_GITLAB_TOKEN = var.TEMPLATE_GITLAB_TOKEN
  GITLAB_BASE_URL       = var.GITLAB_BASE_URL
  GITLAB_TEMPLATE_URL   = var.GITLAB_TEMPLATE_URL
  GITLAB_SERVER_URL     = var.GITLAB_SERVER_URL
}