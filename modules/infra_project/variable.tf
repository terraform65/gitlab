variable "parent_group" {
  type        = string
  description = "The parent group id."
}
variable "visibility" {
  type        = string
  default     = "private"
  description = "The project group Visibility."

  validation {
    condition     = var.visibility == "private" || var.visibility == "public" || var.visibility == "internal"
    error_message = "The visibility must be private, public or internal."
  }
}
variable "GITLAB_TOKEN" {
  default     = "< GITLAB_TOKEN >"
  description = "The gitlab connection token"
}
variable "TEMPLATE_GITLAB_TOKEN" {
  default     = "< GITLAB_TOKEN >"
  description = "The gitlab template connection token"
}
variable "GITLAB_BASE_URL" {
  default     = "https://my.gitlab.server/api/v4/"
  description = "The gitlab API url"
}
variable "GITLAB_TEMPLATE_URL" {
  default     = "gitlab.com"
  description = "The gitlab template url"
}
variable "GITLAB_SERVER_URL" {
  default     = "gitlab.com"
  description = "The gitlab url"
}