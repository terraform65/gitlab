# The data template
data "gitlab_project" "template_project" {
  id = var.project_template_id
}

# Script to push the template 
resource "null_resource" "clone" {
  provisioner "local-exec" {
    command = <<EOT
# Create the clone directory
cd /tmp
mkdir ${gitlab_project.project.id}
cd ${gitlab_project.project.id}
git clone https://oauth2:${var.TEMPLATE_GITLAB_TOKEN}@${var.GITLAB_TEMPLATE_URL}/${data.gitlab_project.template_project.path_with_namespace}
cd ${data.gitlab_project.template_project.path}

# Create the new repository
rm -rf .git
git init
git remote add origin https://oauth2:${var.GITLAB_TOKEN}@${var.GITLAB_SERVER_URL}/${gitlab_project.project.path_with_namespace}

# Launch the script to replace words
./script.sh ${var.global_project_name} ${var.args}

# Suppression du script
rm script.sh

# Commit the instantiated template
git add .
git commit -m "Initial Commit"

# Push the project
git push origin master

# Remove files
cd /tmp
rm -rf cd ${gitlab_project.project.id}
EOT 
  }
}