# Project settings
resource "gitlab_project" "project" {
  name                                             = var.project_name
  description                                      = var.project_description
  namespace_id                                     = var.project_group
  visibility_level                                 = var.visibility
  default_branch                                   = "dev"
  merge_method                                     = "ff"
  only_allow_merge_if_pipeline_succeeds            = var.pipeline_enabled
  only_allow_merge_if_all_discussions_are_resolved = true
  shared_runners_enabled                           = var.project_shared_runner_enabled
  initialize_with_readme                           = false
  container_registry_enabled                       = var.project_registry
  pipelines_enabled                                = var.pipeline_enabled
  packages_enabled                                 = var.project_registry
}