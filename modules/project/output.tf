output "project_id" {
  value       = gitlab_project.project.id
  description = "To output the project id to use it"
}

output "project_path" {
  value       = gitlab_project.project.path_with_namespace
  description = "To output the project path, for exemple to replace the CI project path"
}