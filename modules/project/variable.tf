variable "project_group" {
  type        = string
  default     = "sub_test"
  description = "The project group id."
}
variable "project_name" {
  type        = string
  default     = "test"
  description = "The project name."
}
variable "project_template_id" {
  type        = string
  description = "The project template id"
}
variable "project_shared_runner_enabled" {
  type        = bool
  default     = false
  description = "To enable the project to use gitlab shared runners."
}
variable "project_registry" {
  type        = bool
  default     = false
  description = "To enable the project registry and the project container registry."
}
variable "project_description" {
  type        = string
  description = "The project description."
  default     = ""
}

variable "visibility" {
  type        = string
  default     = "private"
  description = "The project visibility."

  validation {
    condition     = var.visibility == "private" || var.visibility == "public" || var.visibility == "internal"
    error_message = "The visibility must be private, public or internal."
  }
}
variable "pipeline_enabled" {
  type        = bool
  default     = false
  description = "The pipeline enabled property."
}

# Script arguments
variable "global_project_name" {
  description = "The project name to replace with the template script."
}
variable "args" {
  default     = ""
  description = "The template script arguments after the project name argument"
}

# Gitlab config
variable "GITLAB_TOKEN" {
  default     = "< GITLAB_TOKEN >"
  description = "The gitlab connection token"
}
variable "TEMPLATE_GITLAB_TOKEN" {
  default     = "< GITLAB_TOKEN >"
  description = "The gitlab template connection token"
}
variable "GITLAB_BASE_URL" {
  default     = "https://my.gitlab.server/api/v4/"
  description = "The gitlab API url"
}
variable "GITLAB_TEMPLATE_URL" {
  default     = "gitlab.com"
  description = "The gitlab template url"
}
variable "GITLAB_SERVER_URL" {
  default     = "gitlab.com"
  description = "The gitlab url"
}