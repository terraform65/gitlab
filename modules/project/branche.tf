# Script to create the release and dev branches
resource "null_resource" "branch" {
  depends_on = [
    null_resource.clone
  ]
  provisioner "local-exec" {
    command = <<EOT
curl -X POST -H "PRIVATE-TOKEN: ${var.GITLAB_TOKEN}" ${var.GITLAB_BASE_URL}projects/${gitlab_project.project.id}/repository/branches?branch=dev\&ref=master;
curl -X POST -H "PRIVATE-TOKEN: ${var.GITLAB_TOKEN}" ${var.GITLAB_BASE_URL}projects/${gitlab_project.project.id}/repository/branches?branch=release\&ref=master
EOT

  }
}

# Branch protections

# protect the master branch
resource "gitlab_branch_protection" "masterProtect" {
  depends_on = [
    null_resource.branch
  ]
  project            = gitlab_project.project.id
  branch             = "master"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}

# protect the release branch
resource "gitlab_branch_protection" "ReleaseProtect" {
  depends_on = [
    null_resource.branch
  ]
  project            = gitlab_project.project.id
  branch             = "release"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}

# protect the dev branch
resource "gitlab_branch_protection" "DevProtect" {
  depends_on = [
    null_resource.branch
  ]
  project            = gitlab_project.project.id
  branch             = "dev"
  push_access_level  = "no one"
  merge_access_level = "maintainer"
}