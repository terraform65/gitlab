# The parent enabler project
module "enabler_parent_project" {
  source                        = "../project"
  project_group                 = module.enablers_sub_group.group_id
  project_name                  = "parent"
  visibility                    = var.visibility
  project_description           = "The parent enabler project"
  pipeline_enabled              = true
  project_template_id           = "25879763"
  global_project_name           = "parent"
  GITLAB_TOKEN                  = var.GITLAB_TOKEN
  TEMPLATE_GITLAB_TOKEN         = var.TEMPLATE_GITLAB_TOKEN
  GITLAB_BASE_URL               = var.GITLAB_BASE_URL
  GITLAB_TEMPLATE_URL           = var.GITLAB_TEMPLATE_URL
  GITLAB_SERVER_URL             = var.GITLAB_SERVER_URL
  args                          = "${var.MAVEN_GROUP_ID} \"0.0.1-SNAPSHOT\" ${var.CI_PROJECT_PATH} \".gitlab_ci_cd_java_deploy.yml\""
  project_registry              = true
  project_shared_runner_enabled = true
}