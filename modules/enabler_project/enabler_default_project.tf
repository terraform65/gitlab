# To create a default enabler
module "enabler_default_project" {
  for_each = toset(var.enablers)
  source                        = "../project"
  project_group                 = module.enablers_sub_group.group_id
  project_name                  = "${each.value}-enabler"
  visibility                    = var.visibility
  project_description           = "The ${each.value} enabler project"
  pipeline_enabled              = true
  project_template_id           = "25879831"
  global_project_name           = "${each.value}-enabler"
  GITLAB_TOKEN                  = var.GITLAB_TOKEN
  TEMPLATE_GITLAB_TOKEN         = var.TEMPLATE_GITLAB_TOKEN
  GITLAB_BASE_URL               = var.GITLAB_BASE_URL
  GITLAB_TEMPLATE_URL           = var.GITLAB_TEMPLATE_URL
  GITLAB_SERVER_URL             = var.GITLAB_SERVER_URL
  args                          = "${each.value} ${var.MAVEN_GROUP_ID} \"0.0.1-SNAPSHOT\" \"0.0.1-SNAPSHOT\" https://gitlab.com/api/v4/projects/${module.enabler_parent_project.project_id}/packages/maven \"0.0.1-SNAPSHOT\" https://gitlab.com/api/v4/projects/${module.enabler_common_project.project_id}/packages/maven ${var.CI_PROJECT_PATH} \".gitlab_ci_cd_java.yml\""
  project_registry              = true
  project_shared_runner_enabled = true
}