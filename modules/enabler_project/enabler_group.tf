# The enabler group
module "enablers_sub_group" {
  source             = "../project_group"
  project_group      = "enablers"
  project_group_path = "enablers"
  parent_group       = var.parent_group
  visibility         = var.visibility
  description        = "The enablers sub group"
}