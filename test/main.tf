# Configure the GitLab Provider
# Configure the token using the GITLAB_TOKEN environment variable
terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.5.0"
    }
  }
}

provider "gitlab" {
}

module "my_project" {
  source          = "../modules/complete_project"
  GITLAB_TOKEN    = var.GITLAB_TOKEN
  TEMPLATE_GITLAB_TOKEN=var.GITLAB_TOKEN
  GITLAB_TEMPLATE_URL="gitlab.com"
  GITLAB_SERVER_URL="gitlab.com"
  GITLAB_BASE_URL = var.GITLAB_BASE_URL
  visibility="public"
  MAVEN_GROUP_ID = "com.gbournac.test"
  enablers = [ "question", "theme", "test", "upload" ]
  parent= "11583144"
}
variable "GITLAB_TOKEN" {
  default = "< GITLAB_TOKEN >"
}

variable "GITLAB_BASE_URL" {
  default = "https://my.gitlab.server/api/v4/"
}